### **prepare:**

sudo yum update && sudo yum upgrade

**git:**

sudo yum install git

**docker:**

sudo yum check-update

curl -fsSL https://get.docker.com/ | sh

sudo systemctl start docker

sudo systemctl enable docker

sudo usermod -aG docker $(whoami)

**nginx:**

docker run --name nginx13 -p 80:80  -v ~/apps/owson-landing/conf/nginx/conf.d/owson-web.conf:/etc/nginx/conf.d/owson-web.conf -v ~/apps/owson-landing:/webs/owson-web ~/logs:/logs -d  nginx:1.13.12

Para conectar a container:
docker exec -it nginx13 bash



docker run --name nginx13 --link macropolis -p 80:80 -v ~/apps/macropolis-lotes/conf/nginx/conf.d/app.conf:/etc/nginx/conf.d/app.conf -v ~/apps/macropolis-lotes/web:/src -v ~/logs:/logs  -v ~/apps/owson-landing/conf/nginx/conf.d/owson-web.conf:/etc/nginx/conf.d/owson-web.conf -v ~/apps/owson-landing:/webs/owson-web  -d  nginx:1.13.12